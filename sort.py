import random
def getKey(item):
  return item[3]

def write_to_file(name,arr):
	with open(name + '.txt', 'w') as file:
		for i in arr:
			for j in i:
				if(j == '\n'):
					break
				file.write(str(j) + "::")
			file.write('\n')

lst = []
for line in open('ml-1m/ratings.dat'):
	fields = line.split("::")
	abc = fields[3].split("\n")
	fields[3] = abc[0]
	lst.append(fields)
#sorted(lst,key=getKey)
random.shuffle(lst)
write_to_file('sorted_ratings.dat',lst)