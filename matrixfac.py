no_of_users = 943
no_of_items = 1682
no_of_genres = 19

import numpy


def matrix_factorization(R, P, Q, K, steps=5000, alpha=0.0002, beta=0.02):
	Q = Q.T
	for step in xrange(steps):
		for i in xrange(len(R)):
			for j in xrange(len(R[i])):
				if R[i][j] > 0:
					eij = R[i][j] - numpy.dot(P[i,:],Q[:,j])
					for k in xrange(K):
						P[i][k] = P[i][k] + alpha * (2 * eij * Q[k][j] - beta * P[i][k])
						Q[k][j] = Q[k][j] + alpha * (2 * eij * P[i][k] - beta * Q[k][j])
		eR = numpy.dot(P,Q)
		e = 0
		for i in xrange(len(R)):
			for j in xrange(len(R[i])):
				if R[i][j] > 0:
					e = e + pow(R[i][j] - numpy.dot(P[i,:],Q[:,j]), 2)
					for k in xrange(K):
						e = e + (beta/2) * (pow(P[i][k],2) + pow(Q[k][j],2))
		if e < 0.001:
			break
	return P, Q.T


# Rating matrix: Users x Items
R = [[0 for x in xrange(no_of_items+1)] for y in xrange(no_of_users+1)]
print(len(R))
print(len(R[0]))
# Tk -> Known entry set of R
Tk = []

for line in open('ml-100k/u.data'):
	fields = line.split('\t')
	uid = int(fields[0])
	mid = int(fields[1])
	rat = int(fields[2])
	#print(uid, mid)
	R[uid][mid] = rat
	Tk.append((uid,mid))
  
R = numpy.array(R)
N = len(R)
M = len(R[0])
features = 19
P = numpy.random.rand(N,features)
Q = numpy.random.rand(M,features)
nP, nQ = matrix_factorization(R, P, Q, features)
nR = numpy.dot(nP, nQ.T)

for i in range(len(nP)):
	print(np[i])