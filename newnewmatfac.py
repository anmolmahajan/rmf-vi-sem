from __future__ import division
import numpy
import math
import libpmf
import time

no_of_users = 6040
no_of_items = 3952
no_of_genres = 19


nR = [[0 for x in xrange(no_of_items+1)] for y in xrange(no_of_users+1)]
"""
def matrix_factorization(R, P, Q, K, steps=1, alpha=0.0002, beta=0.02):
	Q = Q.T
	for step in xrange(steps):
		for i in xrange(len(R)):
			for j in xrange(len(R[i])):
				if R[i][j] > 0:
					eij = R[i][j] - numpy.dot(P[i,:],Q[:,j])
					for k in xrange(K):
						P[i][k] = P[i][k] + alpha * (2 * eij * Q[k][j] - beta * P[i][k])
						Q[k][j] = Q[k][j] + alpha * (2 * eij * P[i][k] - beta * Q[k][j])
		eR = numpy.dot(P,Q)
		e = 0
		for i in xrange(len(R)):
			for j in xrange(len(R[i])):
				if R[i][j] > 0:
					e = e + pow(R[i][j] - numpy.dot(P[i,:],Q[:,j]), 2)
					for k in xrange(K):
						e = e + (beta/2) * (pow(P[i][k],2) + pow(Q[k][j],2))
		if e < 0.001:
			break
	return P, Q.T
"""



def matrix_factorization(R,K,offset):
	model = libpmf.train(R, '-k ' + str(K) + ' -l ' + str(offset) + ' ' +'-N 1 ' + '-n 8 ' + '-t 50 ' + '-T 50')
	return model
	
def write_to_file(name,arr):
	with open(name + '.txt', 'w') as file:
		for i in arr:
			for j in i:
				file.write(str(j) + '\t')
			file.write('\n')
  
def incremental(A, B, Ti, R):
	a = b = 0
	for i in range(len(Ti)):
		for j in range(len(Ti[i])):
			if(Ti[i][j] != 0):
				R[i][j] = Ti[i][j]
				a += 1
				if(a == A):
					a = 0
					b = b+A
					if(b>B):
						a = 0
						b = 0
						model = matrix_factorization(R, 10,0.05)
						global nR
						nR = numpy.dot(model['W'],model['H'].T)
	

				

def predict(nR):
	for i in range(len(nR)):
		for j in range(len(nR[i])):
			print(str(nR[i][j]) + ' ')
		print('\n')
		
def metric_calc(Tv):
	sums = 0
	size_Tv = 0
	mae = 0
	count_common = 0
	count_pref = 0
	count_rec = 0
	for i in range(len(Tv)):
		for j in range(len(Tv[i])):
			if(Tv[i][j] != 0):
				size_Tv += 1
				global nR
				sums += math.pow((nR[i][j] - Tv[i][j]),2)
				mae += abs(nR[i][j] - Tv[i][j])
				if(Tv[i][j] > 3):
				 	if(nR[i][j] > 3):
						count_common += 1
					count_pref += 1
	print count_common
	print count_pref
	sums = sums / size_Tv
	sums = math.sqrt(sums)
	return sums, mae/size_Tv, count_common/count_pref, count_common
  
def countallrecomm(Ts,Ti):
	countallrec = 0
	global nR
	for i in range(len(nR)):
		for j in range(len(nR[i])):
			if(Ts[i][j] == 0 and Ti[i][j] == 0 and int(nR[i][j]) > 3):
				countallrec += 1
	print countallrec
	return countallrec

def emplist(lst):
	del lst[:]
	lst[:] = []
	return lst

R = [[0 for x in xrange(no_of_items+1)] for y in xrange(no_of_users+1)]

print("Reading training data")
for line in open('dataset/training.dat'):
	fields = line.split("::")
	uid = int(fields[0])
	mid = int(fields[1])
	rat = int(fields[2])
	#print(uid, mid)
	R[uid][mid] = rat

s_s_time = time.time()
R = numpy.array(R)
model = matrix_factorization(R, 10, 0.05)
nR = numpy.dot(model['W'],model['H'].T)
s_e_time = time.time()

print("\nReading incremental data")
Ti = [[0 for x in xrange(no_of_items+1)] for y in xrange(no_of_users+1)]
for line in open('dataset/incremental.dat'):
	fields = line.split("::")
	uid = int(fields[0])
	mid = int(fields[1])
	rat = int(fields[2])
	Ti[uid][mid] = rat

print("Making incremental changes\t Computing time\n\n")
print("-----------------------")

start_time = time.time()
A=200
B=40000
incremental(A,B,Ti,R)
print("-----------------------\n\n")

end_time = time.time()


print("Reading Testing data")
Tv = [[0 for x in xrange(no_of_items+1)] for y in xrange(no_of_users+1)]
for line in open('dataset/validation.dat'):
	fields = line.split("::")
	uid = int(fields[0])
	mid = int(fields[1])
	rat = int(fields[2])
	Tv[uid][mid] = rat


incr_time = (end_time-start_time)/6
static_time = s_e_time- s_s_time
metric_list = metric_calc(Tv)
print("\n")
print("RMSE: "+str(metric_list[0]))
print("MAE: "+str(metric_list[1]))
print("Recall: "+str(metric_list[2]))
pr = metric_list[3]/countallrecomm(R,Ti)
print("Precision: "+str(pr))
print("Incremental Time: "+str(incr_time)+" seconds")
print("Total Time: "+str(incr_time+static_time)+ "seconds\n")